

package hadoop_2;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

 public  class InvertedIndexReducer
            extends Reducer<Text, IntWritable, Text, VectorWritable> {

        private VectorWritable docsWritable;

        @Override
        public void reduce(Text key, Iterable<IntWritable> values,Context context
        ) throws IOException, InterruptedException {
            int n = context.getConfiguration().getInt("N", 0); // get number of documents 
            docsWritable = new VectorWritable(); //initialize VectorWritable 
            ArrayList<IntWritable> docs = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                docs.add(new IntWritable(0)); // for each position set value to zero 
            }

            for (IntWritable val : values) {
                int position = val.get() - 1; // position of word founded 
                docs.set(position, new IntWritable(1)); // set position of array that word founded to ( One )
            }

            docsWritable.set(docs); //set ArrayList to VectorWritable
            context.write(key, docsWritable);
        }

    }

