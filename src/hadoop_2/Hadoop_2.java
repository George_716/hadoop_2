package hadoop_2;

import java.io.DataOutputStream;
import java.net.URI;
import java.util.ArrayList;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;


public class Hadoop_2 {

   
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        //Five paths 
        if (otherArgs.length != 5) {
            System.err.println("Usage: Kmeans <input> <output> <total docs> <clusters> <k-means iterations>");
            System.exit(2);
        }
         FileSystem hdfs =FileSystem.get(conf); // get filesystem 
        int totalDocs = Integer.valueOf(otherArgs[2]); // number of docs 
        int numberOfIterations = Integer.valueOf(otherArgs[4]); // number of kmeans iterations 
        conf.setInt("N", totalDocs); // set total number of docs 
        DistributedCache.addCacheFile(Hadoop_2.class.getResource("stopwords.txt").toURI(), conf); // upload file with stopwords 
        //set first job 
        Job job = new Job(conf, "Inverted Index");
        job.setJarByClass(Hadoop_2.class);
        job.setMapperClass(InvertedIndexMapper.class);
        job.setReducerClass(InvertedIndexReducer.class);
        job.setMapOutputKeyClass(Text.class); // set Map output key format to Text
        job.setMapOutputValueClass(IntWritable.class); // set Map output Value format to IntWritable
        job.setOutputKeyClass(Text.class); // set output Key format to Text 
        job.setOutputValueClass(VectorWritable.class); // set output Value format to VectorWritable
        FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
        String pathString = otherArgs[1]+"/InvertedIndex";
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        FileOutputFormat.setOutputPath(job,new Path(pathString));// set output folder 
        
        //If First Job is finished take output results and use them as input to second Job
        if (job.waitForCompletion(true)) {
                System.out.println("----- > CREATED INVERTED INDEX  !!!!!  < ------ ");
                int clusters = Integer.valueOf(otherArgs[3]); // retrieve number of clusters 
                Path centersFile = new Path(otherArgs[1]+"/Centers.txt"); // path for file that stores centers
                
                try (DataOutputStream fs = hdfs.create(centersFile)) { // open file for output 
                  for(int i=0;i<clusters;i++){ // for each cluster 
                    ArrayList<FloatWritable> list = new ArrayList<>(); // create list 
                    for(int y=0;y<totalDocs;y++){ // for each doc 
                        float random = (float) Math.random(); // pick random number between zero and one 
                       float changePrecision =  Math.round(random * 1000); // precision = 3 
                       changePrecision = changePrecision / 1000; // precision = 3 
                       random  = changePrecision; // change value of random number with precision  = 3
                        list.add(new FloatWritable(random)); // add number to list 
                    }
                    CenterWritable center = new CenterWritable(list,new IntWritable(i)); // create center with id and list
                    center.Firstwrite(fs); // write center to file 
                  }
                }
               DistributedCache.addCacheFile(new URI(otherArgs[1]+"/Centers.txt"), conf); // upload file with centers
               
              for(int i =0;i<numberOfIterations;i++){
                conf.setInt("cacheFileRetrieval",i+1); // set position of cache file to retrieve each map

                // set another job 
                Job job1 = new Job(conf, "K-means");
                job1.setJarByClass(Hadoop_2.class);
                job1.setMapperClass(KMeansMapper.class);
                job1.setReducerClass(KMeansReducer.class);
                job1.setMapOutputKeyClass(IntWritable.class); // set Map output key format to Text
                job1.setMapOutputValueClass(VectorWritable.class); // set Map output Value format to IntWritable
                job1.setOutputKeyClass(CenterWritable.class); // set output Key format to Text 
                job1.setOutputValueClass(Text.class); // set output Value format to VectorWritable
                job1.setInputFormatClass(SequenceFileInputFormat.class);
                FileInputFormat.addInputPath(job1,new Path(pathString));
                job1.setOutputFormatClass(CustomCentersOutputFormat.class);
                FileOutputFormat.setOutputPath(job1,new Path(otherArgs[1]+"/Kmeans"+(i+1))); // set output folder for each iteration
                
                if(job1.waitForCompletion(true)){
                    System.out.println("K-means Clustering Iteration "+(i+1)+" Done !!!!");
                    DistributedCache.addCacheFile(new URI(FileOutputFormat.getOutputPath(job1)+"/newCenters.txt"), conf);
                    if(i == numberOfIterations -1 ){
                      
                       conf.setInt("cacheFileRetrieval",i+2); // set position of cache file to retrieve each map

                         // set final job 
                        Job job2 = new Job(conf, "K-means Final Step");
                        job2.setJarByClass(Hadoop_2.class);
                        job2.setMapperClass(FinalPhaseMapper.class);
                        job2.setReducerClass(FinalPhaseReducer.class);
                        job2.setMapOutputKeyClass(IntWritable.class); // set Map output key format to Text
                        job2.setMapOutputValueClass(Text.class); // set Map output Value format to IntWritable
                        job2.setOutputKeyClass(IntWritable.class); // set output Key format to Text 
                        job2.setOutputValueClass(Text.class); // set output Value format to VectorWritable
                        job2.setInputFormatClass(SequenceFileInputFormat.class); //set input format 
                        FileInputFormat.addInputPath(job2,new Path(pathString));

                        FileOutputFormat.setOutputPath(job2,new Path(otherArgs[1]+"/finalClusters")); // set output folder for each iteration
                        if(job2.waitForCompletion(true)){
                            System.out.println("Kmeans Clustering Finished ......"); // clustering finished 
                        }
                    }
                }
               }
              
            }
        }

        
    

}
