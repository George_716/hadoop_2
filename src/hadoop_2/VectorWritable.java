package hadoop_2;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

public final class VectorWritable  implements Writable {

  private ArrayList<IntWritable> vector;

  //method returns arraylist ( vector ) 
  public ArrayList<IntWritable> get() {
    return vector;
  }  
// setter
  public void set(ArrayList<IntWritable> vector) {
    this.vector = vector;
  }

  public VectorWritable() {
      
      super();
      vector = new ArrayList<>();
  }

  public VectorWritable(ArrayList<IntWritable> v) {
    vector = v;
  }
  
  //method returns arraylist size ( vector ) 
  public int size(){
      return vector.size();
  }
  
  //method writes data to file 
  @Override
  public void write(DataOutput out) throws IOException {
     
      out.writeInt(vector.size());
      for(IntWritable value : vector){
        out.writeInt(value.get());
     }
  }
//method reads data from file 
  @Override
  public void readFields(DataInput in) throws IOException {
        int size = in.readInt();
        vector = new ArrayList<>(size);
        for(int i=0;i<size;i++){
            IntWritable number = new IntWritable();
            number.set(in.readInt());
            vector.add(number);
        }
  }

 
  

}
