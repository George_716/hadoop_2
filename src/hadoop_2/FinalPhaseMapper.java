

package hadoop_2;

import java.io.IOException;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.net.URI;
import java.util.ArrayList;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public  class FinalPhaseMapper
            extends Mapper<Text, VectorWritable,IntWritable,Text> {
 
        public ArrayList<CenterWritable> centers = new ArrayList<>(); // list of CenterWritable objects
       
        private IntWritable calculateDistance(ArrayList<CenterWritable> centers, VectorWritable vector) {
            double min = -1; // minimum value 
            int minPosition = -1; // minimum position
            if(vector == null){
                vector = new VectorWritable(); // initialize vector
            }
            ArrayList<IntWritable> currentVector = vector.get();
            for (int i = 0; i < centers.size(); i++) { // for each center

                //calculate the sums of the mathematical type
                float sum1 = 0, sum2 = 0, sum3 = 0;
                ArrayList<FloatWritable> currentCenter = centers.get(i).get();
                for (int j = 0; j < vector.size(); j++) {
                    sum1 += currentCenter.get(j).get() * currentVector.get(j).get();
                    sum2 += pow(currentCenter.get(j).get(), 2);
                    sum3 += pow(currentVector.get(j).get(), 2);
                }

                //calculate the distance
                double distance = (1 - sum1) / (sqrt(sum2) * sqrt(sum3));//CHANGE

                //check if the current distance is smaller than the previous ones
                if(min == -1){ // initialize min value and minPosition
                    min = distance;
                    minPosition = i;
                }
                else if (distance < min) { // if value is smaller than minimum change it 
                    min = distance;
                    minPosition = i;
                }
            }

            //return the ID of the closest center
            return  centers.get(minPosition).getID() ;
        }
        
        // open Centers.txt file and read all centers . Store them to ArrayList
        @Override
        public void setup(Mapper.Context context) throws IOException{
            int cacheFilePosition = context.getConfiguration().getInt("cacheFileRetrieval", 0); // get cache file position
            FileSystem fs = FileSystem.get(context.getConfiguration()); // get filesystem
            URI[] uriArray = DistributedCache.getCacheFiles(context.getConfiguration());// get stop Words Text File
            FSDataInputStream in = fs.open(new Path(uriArray[cacheFilePosition].toString())); // open file for input
            while(in.available() != 0){ // while we can read from file 
              CenterWritable center = new CenterWritable(); // initialize CenterWritable object 
              center.readFields(in);  // 
              centers.add(center); // add data to list 
            }
        }

        @Override
        public void map(Text key, VectorWritable value, Context context
        ) throws IOException, InterruptedException {            
            VectorWritable currentVector = value; //vector of current word 
            IntWritable closestCenterId = calculateDistance(centers, currentVector);//calculate distance from clusters
             context.write(closestCenterId, key);

        }
    }