

package hadoop_2;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public  class KMeansReducer
            extends Reducer<IntWritable, VectorWritable,CenterWritable,Text> {

        //private VectorWritable docsWritable;
        private IntWritable initializeVectors(ArrayList<VectorWritable> vectors) {
            //TODO: read the input from the previous mapper and create the vectors

            //TODO: The function should return the key, which should be the team ID
            return null;
        }

        @Override
        public void reduce(IntWritable key, Iterable<VectorWritable> values, Context context
        ) throws IOException, InterruptedException {
            //TODO: read the output of the mapper as input and set the following values
            int numberOfDocuments = context.getConfiguration().getInt("N", 0); //SET THE NUMBER OF DOCUMENTS
            CenterWritable center;
            ArrayList<VectorWritable> vectors = new ArrayList<>(); // 
            IntWritable teamID; // store cluster id 
            teamID = initializeVectors(vectors);

            //creation of the new center
            ArrayList<FloatWritable> newCenter = new ArrayList<>();
            for (int j = 0; j < numberOfDocuments; j++) {
                FloatWritable zero = new FloatWritable(0);
                newCenter.add(zero);
            }

            int size  = 0;

            //calculate sum
            for (VectorWritable currentVector : values) {
                //VectorWritable currentVector = vectors.get(j);
                for (int k = 0; k < currentVector.size(); k++) {

                    //calculate result
                    float value1 = newCenter.get(k).get();
                    int value2 = currentVector.get().get(k).get();
                    float result = value1 + value2;
                    newCenter.set(k, new FloatWritable(result));
                }
                size++; // 
            }

            center = new CenterWritable();
            center.set(newCenter,key);

            //divide center by size
            center.divideBySize(size);
            context.write(center,new Text(""));
            
        }

    }

