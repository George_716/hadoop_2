

package hadoop_2;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public  class FinalPhaseReducer
            extends Reducer<IntWritable, Text,IntWritable, Text> {
        StringBuilder builder; // stringbuilder
        Text text;
        @Override
        public void reduce(IntWritable key, Iterable<Text> values, Context context
        ) throws IOException, InterruptedException {
            builder = new StringBuilder();
            text = new Text();
            for(Text value : values){
                builder.append(value.toString()+" ");
            }
            text.set(builder.toString());
           context.write(key,text);
        }
    }
