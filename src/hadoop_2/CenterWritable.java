
package hadoop_2;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;


public class CenterWritable implements Writable{

    private IntWritable centerID;
    private ArrayList<FloatWritable> center;
    
    public CenterWritable(){
        center=new ArrayList<>();
    }
    
    public CenterWritable(ArrayList<FloatWritable> center,IntWritable centerID){
        this.center=new ArrayList<>();
        this.center=center;
        this.centerID=centerID;
    }
    
    
    public void divideBySize(int size){
        for(int i=0;i<center.size();i++){
            float dividedValue=(center.get(i).get())/size;
            float changePrecision =  Math.round(dividedValue * 1000); // precision = 3 
                       changePrecision = changePrecision / 1000; // precision = 3 
                       dividedValue = changePrecision; // change value of random number with precision  = 3
            center.set(i, new FloatWritable(dividedValue));
        }
    }
    //method returns arraylist ( center )
    public ArrayList<FloatWritable> get(){
        return center;
    }
    
    //setter 
    public void set(ArrayList<FloatWritable> center,IntWritable centerID){
        this.center=center;
        this.centerID=centerID;
    }
    
    public IntWritable getID(){
        return this.centerID;
    }
    //method writes data to file 
    public void Firstwrite(DataOutput out) throws IOException {
      out.writeBytes(String.valueOf(centerID)+": ");
      out.writeBytes(String.valueOf(center.size())+" ");
      int counter = 0;
      for(FloatWritable value : center){
        out.writeBytes(String.valueOf(value.get()));
        if(counter < center.size()-1){
            out.writeBytes(",");
        }
        counter++;
      }
      out.writeBytes("\r\n"); 
    }
    
    //method writes data to file 
    @Override
    public void write(DataOutput out) throws IOException {
       int counter = 0;
       out.writeInt(centerID.get());
       out.writeUTF(": ");
       out.writeInt(center.size());
       out.writeUTF(" ");
      for(FloatWritable value : center){
        out.writeFloat(value.get());
        if(counter < center.size()-1){
          out.writeUTF(",");
        }
        counter++;
      }
      
    }
    //method reads data from file 
    @Override
    public void readFields(DataInput in) throws IOException {
       String retrievedLine = in.readLine(); // read line of file 
         if(retrievedLine != null){ // if not null(not reached end of file)
           String firstSplit[] = retrievedLine.split(": "); // split to key and value (Text and VectorWritable)
           String secondSplit[] = firstSplit[1].split(" ");//split value to pass first whitespace
           int id = Integer.parseInt(firstSplit[0]);
           centerID = new IntWritable(id); // set value of key 
           int size = Integer.parseInt(secondSplit[0]);
           String thirdSplit[] = secondSplit[1].split(","); // split VectorWritable to pieces 
           center = new ArrayList<>(size);
           for(int i=0;i<size;i++){ // for each splitted VectorWritable piece 
               float val = Float.parseFloat(thirdSplit[i]); // take int value of String 
               center.add(new FloatWritable(val)); // add int value to VectorWritable( ArrayList ) 
           }
         }
    }
    
    //method returns size of arraylist( center ) 
    public int size(){
        return this.center.size();
    }
    
}
