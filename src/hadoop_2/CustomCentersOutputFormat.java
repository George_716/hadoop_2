

package hadoop_2;

import java.io.IOException;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 *
 * @author geohar
 */
public class CustomCentersOutputFormat extends FileOutputFormat<CenterWritable,Text> {
    
    @Override
    public RecordWriter<CenterWritable,Text> getRecordWriter(TaskAttemptContext tac) throws IOException, InterruptedException {
     //get the current path
     Path path = FileOutputFormat.getOutputPath(tac);
     
     //create the full path with the output directory plus our filename
     Path fullPath = new Path(path,"newCenters.txt");

     //create the file in the file system
     FileSystem fs = path.getFileSystem(tac.getConfiguration());
     FSDataOutputStream fileOut = fs.create(fullPath, tac);

     //create our record writer with the new file
     return new CustomCentersRecordWriter(fileOut);
    
    }
}
