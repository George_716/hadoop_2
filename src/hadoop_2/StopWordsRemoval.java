

package hadoop_2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class StopWordsRemoval {
   private final String path ; // path of stopWords text file 
   private final ArrayList<String> stopWords; // ArrayList that we will save each word from file 
    public StopWordsRemoval(String stopWordsPath){
        path = stopWordsPath;
        stopWords = new ArrayList<>();
    }
    //method returns how many lines there are to text file
    public int returnTextSize() throws FileNotFoundException, IOException{
        FileReader filereader = new FileReader(path);
        int numberOfLines ;
        try (BufferedReader reader = new BufferedReader(filereader)) {
            String line;
            numberOfLines = 0; 
            while((line = reader.readLine()) != null){
                numberOfLines++;
            }
        }
        return numberOfLines;
    }
    
    //Method returns one(1) if word exists to stopWords ArrayList , or zero(0) if not exists
    public int checkIfWordIsStopWord(String word) throws FileNotFoundException, IOException{
        FileReader filereader = new FileReader(path);
        BufferedReader reader = new BufferedReader(filereader);
        int numOfLines =   returnTextSize(); // return number of lines 
        for(int i=0;i<numOfLines;i++){
          stopWords.add(reader.readLine()); //add each word to ArrayList
        }
        for (String stopWord : stopWords) {
            if(stopWord.equals(word)){
                return 1; // Given word containing to Stop Words ArrayList. 
            }
        }
        return 0; // Given word not containing to Stop Words ArrayList.
    }
    
}
