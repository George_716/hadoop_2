

package hadoop_2;

import java.io.IOException;
import java.net.URI;
import java.util.StringTokenizer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

 public  class InvertedIndexMapper
            extends Mapper<Object, Text, Text, IntWritable> {

        private static final IntWritable docid = new IntWritable();
        private final Text word = new Text();
        public static final Log LOG = LogFactory.getLog(InvertedIndexMapper.class);

        @Override
        public void map(Object key, Text value, Mapper.Context context
        ) throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString()); // Tokenize String to smaller
            URI[] uriArray = DistributedCache.getCacheFiles(context.getConfiguration());// get stop Words Text File
            StopWordsRemoval stopWords = new StopWordsRemoval(uriArray[0].getPath()); // initialize StopWordsRemoval
            while (itr.hasMoreTokens()) {

                FileSplit fileSplit = (FileSplit) context.getInputSplit(); // we take InputSplit as FileSplit format
                String filename = fileSplit.getPath().getName(); // filename to String format
                int docId = Integer.parseInt(filename); // export int value docId from String
                Stemmer stemmer = new Stemmer(); // initialize Stemmer object
                String temporaryWord = itr.nextToken().toLowerCase(); // retrieve Word to lowerCase
                temporaryWord = temporaryWord.replaceAll("\\W", " "); // remove special characters that not needed

                String[] splitWord = temporaryWord.split(" "); // split words with blank character seperator
                for (String splittedWord : splitWord) { // for each word  if it is not empty save it 
                    if (splittedWord.isEmpty()) {
                        continue; // we founded empty string .. Move to next String
                    } else if (stopWords.checkIfWordIsStopWord(splittedWord) == 1) {
                        continue; // word containing to Stop Words ArrayList . Move to next String
                    }
                    word.set(splittedWord); // set Text value 
                    stemmer.add(word.toString().toCharArray(), word.toString().length());  // set word to stem
                    stemmer.stem(); // call method to stem
                    word.set(stemmer.toString()); // set word value with stemmer result
                    docid.set(docId); // set Document id with number that retrieved
                    context.write(word, docid);
                }
            }
        }
    }

