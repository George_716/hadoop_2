

package hadoop_2;

import java.io.DataOutputStream;
import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;


public class CustomCentersRecordWriter extends RecordWriter<CenterWritable,Text> {
    private final DataOutputStream out; // output stream for write 

    public CustomCentersRecordWriter(DataOutputStream stream) {
        out = stream;
         
    }
    @Override
    public void write(CenterWritable k,Text v) throws IOException, InterruptedException {
         //write out our key
        out.writeBytes(k.getID().toString() + ": ");
        out.writeBytes(k.get().size()+" ");
        //loop through all values associated with our key and write them with commas between
        for (int i=0; i<k.get().size(); i++) {
            if (i>0){
              out.writeBytes(",");
            }
            
            out.writeBytes(String.valueOf(k.get().get(i).get()));
        }
        out.writeBytes("\r\n");  
    }
    

    @Override
    public void close(TaskAttemptContext tac) throws IOException{
      
        out.close(); // close stream
       
    }
}
